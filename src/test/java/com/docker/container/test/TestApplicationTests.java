package com.docker.container.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.MySQLContainer;

import static org.junit.Assert.assertEquals;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestApplicationTests {

	@Autowired
	private MyService myService;

	@Rule
	public MySQLContainer postgresContainer = new MySQLContainer();

	@Test
	public void contextLoads() {

		String jdbcUrl = postgresContainer.getJdbcUrl();
		String username = postgresContainer.getUsername();
		String password = postgresContainer.getPassword();
		log.info("jdbcUrl: {}", jdbcUrl);
		log.info("username: {}", username);
		log.info("password: {}", password);

		assertEquals(myService.getValue(), "MY_VALUE");
	}

}
