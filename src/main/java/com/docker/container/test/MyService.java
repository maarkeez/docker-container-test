package com.docker.container.test;

import org.springframework.stereotype.Service;

@Service
public class MyService {

    public String getValue(){
        return "MY_VALUE";
    }
}
